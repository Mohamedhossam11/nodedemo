const express = require('express')
const router = express.Router()

const { hello, tryingPromise } = require('../controllers/hello.controller')

const { debugHeader } = require('../helpers/helpers')

// router.post('/hello', (req, res) => {
//   console.log('hello world')
//   return res.json({ msg: 'hello' })
// })
router.post('/hello', hello)
router.post('/tryingpromise', debugHeader, tryingPromise)

module.exports = router
