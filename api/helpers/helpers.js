const debugHeader = (req, res, next) => {
  console.log(req.headers)
  return next()
}

module.exports = { debugHeader }
