const axios = require('axios')

const hello = (req, res) => {
  const { input } = req.body
  console.log(input)
  res.json({ msg: 'hello from controller' })
}

const tryingPromise = async (req, res) => {
  let output = 1
  await axios({
    method: 'post',
    url: 'http://localhost:5000/hello/hello',
    data: {
      input: 'Call from myself',
    },
  }).then((res) => (output = 0))
  res.json({ msg: output })
}

module.exports = { hello, tryingPromise }
