const express = require('express')
const cors = require('cors')
const allRoutes = require('express-list-endpoints')
const bodyParser = require('body-parser')

const app = express()

const hello = require('./api/routers/hello.router')

// import db configuration
// const sequelize = require('./config/DBConfig')

// add other middleware
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// test postgres connection
// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connected to postgres')
//   })
//   .catch((err) => {
//     console.error('Unable to connect to postgres', err)
//   })

const explore = (req, res) => {
  const routes = allRoutes(app)
  const result = {
    ServiceList: [],
  }

  routes.forEach((route) => {
    const name = route.path.split('/')[5]
    result.ServiceList.push({
      Service: {
        name,
        fullUrl: `${route.path}`,
      },
    })
  })
  return res.json(result)
}

app.use('/hello', hello)
app.use('/explore', explore)

app.get('/hello', (req, res) => {
  console.log('hello world')
  return res.json({ msg: 'hello' })
})

app.use((req, res) => {
  res.status(404).send({ err: 'No such url' })
})

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Origin', 'GET, POST, OPTIONS')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, X-Auth-Token, Accept'
  )
  next()
})

// const eraseDatabaseOnSync = false

// sequelize
//   .sync({ force: eraseDatabaseOnSync })
//   .then(() => console.log('Synced models with database'))
//   .then(() => {
//     if (eraseDatabaseOnSync) {
//       eraseDatabaseOnSyncContacts()
//       populate_admins()
//       populate_users()
//       populate_pricing()
//     }
//   })
//   .catch((error) => console.log('Could not sync models with database', error))

const port = 5000
app.listen(port, () => console.log(`Server up and running on ${port}`))
